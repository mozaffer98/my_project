

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyTextField extends StatelessWidget {
final Function validator,onChanged;
final String Name,hint_text;
final bool isPassword,  isnum;
final Widget icon;
final TextEditingController ctrl;

   MyTextField({Key key, this.validator, this.onChanged, this.Name, this.isPassword, this.hint_text, this.icon, this.ctrl, this.isnum}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      margin: EdgeInsets.only(top:20),
      child:TextFormField(
        controller: ctrl,
        textDirection: TextDirection.ltr,
        keyboardType: isnum==false?TextInputType.text:TextInputType.phone,
        obscureText: isPassword,
        textAlign: TextAlign.right,
        decoration: InputDecoration(
          hintText: hint_text,
          prefixIcon:icon,
          border: OutlineInputBorder(),
        ),

      ) ,
    );;
  }
}
