import 'package:e_commerce/main.dart';
import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  final title;
  final function;
  final mdw ;
  final heigth ;

  const MyButton({Key key, this.function, this.title , this.mdw, this.heigth}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
        color: MainColor,
        height: heigth,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        onPressed: function,
        minWidth: mdw/2.0,
        child: Text("$title" , style: TextStyle(color: Colors.white , fontSize: 18),));
  }
}
