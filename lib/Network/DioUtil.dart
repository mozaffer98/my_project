import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:dio/dio.dart';

import 'auth_inteceptor.dart';


class DioUtil {

 static Dio createDio(){
    BaseOptions _baseOptions = BaseOptions(
        receiveTimeout: 30000,
        connectTimeout: 11000, //receiveDataWhenStatusError: true,
        followRedirects: false,

        validateStatus: (status) {return status > 0;} ,
        baseUrl: "http://finalpromoahmed-001-site1.btempurl.com/api/");
        Dio _dio = Dio(_baseOptions);
       _dio.interceptors.add(AuthInterceptor());
    return _dio;
  }
}













