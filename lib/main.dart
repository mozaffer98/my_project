
import 'package:e_commerce/auth/Login.dart';
import 'package:flutter/material.dart';

var MainColor=Colors.teal;

void main() async {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '',
      theme: ThemeData(
        primarySwatch: MainColor,
      ),
      home:   Login(),
      debugShowCheckedModeBanner: false,
    );
  }
}
