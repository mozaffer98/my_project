
import 'package:e_commerce/Component/BezierContainer.dart';
import 'package:e_commerce/Component/MyTextField.dart';
import 'package:e_commerce/Network/DioUtil.dart';
import 'package:e_commerce/auth/Register.dart';
import 'package:e_commerce/main.dart';
import 'package:e_commerce/model/User.dart';
import 'package:e_commerce/screens/Home.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget {

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController email_ctrl=TextEditingController();
  TextEditingController password_ctrl=TextEditingController();
  int radioGroup = 2;
  bool isloading=false;

  Widget _submitButton() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10),
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          isloading == false
              ?  GestureDetector(
            onTap: Login,
            child: Container(
              height: 50,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: MainColor,
                        offset: Offset(2, 4),
                        blurRadius: 5,
                        spreadRadius: 2)
                  ],
                  gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [MainColor,MainColor.shade100])),
              child: Center(
                child: Text(
                  'Login',
                  style: TextStyle(fontSize: 16, color: Colors.white),

                ),
              ),

            ),
          )
              : Center(
            child: CircularProgressIndicator(),
          ),
        ],
      ),
    ) ;
  }
  Widget _divider() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 20,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Divider(
                thickness: 1,
              ),
            ),
          ),
          Text('or'),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Divider(
                thickness: 1,
              ),
            ),
          ),
          SizedBox(
            width: 20,
          ),
        ],
      ),
    );
  }

  Widget _createAccountLabel() {
    return InkWell(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => Register()));
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 20),
        padding: EdgeInsets.all(15),
        alignment: Alignment.bottomCenter,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Dont have account?',
              style: TextStyle(fontSize: 13, fontWeight: FontWeight.w600),
            ),

            SizedBox(
              width: 10,
            ),
            Text(
              'Register',
              style: TextStyle(
                  color: Color(0xfff79c4f),
                  fontSize: 13,
                  fontWeight: FontWeight.w600),
            ),
          ],
        ),
      ),
    );
  }

  Widget _title() {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
          text: 'Lo',
          style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w700,
              color: Color(0xffe46b10)
          ),
          children: [
            TextSpan(
              text: 'gi',
              style: TextStyle(color: Colors.black, fontSize: 30),
            ),
            TextSpan(
              text: 'n',
              style: TextStyle(color: Color(0xffe46b10), fontSize: 30),
            ),
          ]),
    );
  }

  Widget _emailPasswordWidget() {
    return Column(
      children: <Widget>[
        MyTextField(Name: "User Name",isnum: false,hint_text: "User Name", ctrl: email_ctrl,isPassword: false),
        SizedBox(height: 20),
        MyTextField(Name: "Pass Word",isnum: false,hint_text: "Pass Word",ctrl: password_ctrl,isPassword: true),
      ],
    );
  }
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
      key: scaffoldKey,
        body: Container(
          height: height,
          child: Stack(
            children: <Widget>[
              Positioned(
                  top: -height * .15,
                  right: -MediaQuery.of(context).size.width * .4,
                  child: BezierContainer()),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: height * .2),
                      _title(),
                      SizedBox(height: 50),
                      _emailPasswordWidget(),
                      SizedBox(height: 20),
                      _submitButton(),
                      _divider(),
                      SizedBox(height: height * .055),
                      _createAccountLabel(),
                    ],
                  ),
                ),
              ),

              // Positioned(top: 40, left: 0, child: _backButton()),
            ],
          ),
        ));
  }

  void Login() async {
    if (email_ctrl.text.isEmpty ||password_ctrl.text.isEmpty) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("please fill all fields"),
        ),
      );
    }
    else{
      setState(() {isloading = true;});
      DioUtil.createDio().post("UsersRigester/Login",data: {"Email": email_ctrl.text,"Password":password_ctrl.text}).then((response) {
        if(response.statusCode == 200)
        {
          setState(() {isloading = false;});
          var body=User.fromJson(response.data);
          if(body.id!=0)
            {
              Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext ctx)=>Home()), (route) => false);
            }
          else  if(body.id==0)
            {
              scaffoldKey.currentState.showSnackBar(
                SnackBar(
                  content: Text("wrong user name or password"),
                ),
              );
            }
        }
        else {
          setState(() {isloading = false;});
          scaffoldKey.currentState.showSnackBar(
            SnackBar(
              content: Text("wrong user name or password"),
            ),
          );
        }
      });
    }

  }
}