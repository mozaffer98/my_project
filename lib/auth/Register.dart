

import 'package:e_commerce/Component/MyTextField.dart';
import 'package:e_commerce/Component/mybutton.dart';
import 'package:e_commerce/Network/DioUtil.dart';
import 'package:e_commerce/auth/Login.dart';
import 'package:e_commerce/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';



String p = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

 bool isMale = true;
class Register extends StatefulWidget {
  const Register({Key key}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  bool isLoading = false;
  TextEditingController _UserName_Ctrl = TextEditingController();
   TextEditingController _Email_Ctrl = TextEditingController();
  TextEditingController _UserPhone_Ctrl = TextEditingController();
  TextEditingController _UseAge_Ctrl = TextEditingController();
  TextEditingController _UserPassWord_Ctrl = TextEditingController();
  TextEditingController _UserPassWordConfirm_Ctrl = TextEditingController();

  List<String> gender=["man","women"];
  var _gender;

  Map<String, int> type={"volunteer":1,"disabled":2};
  var _type;

  Widget _buildAllTextFormField() {
    return Container(
        margin: EdgeInsets.all(10),
        child:
        Column(
          children: [
            MyTextField(Name: "User name",
              hint_text: "User name",
              isPassword: false,
              isnum: false,
              icon: Icon(Icons.person),
              ctrl: _UserName_Ctrl,),

            MyTextField(Name: "Email",
              hint_text: "Email",
              isPassword: false,
              isnum: false,
              icon: Icon(Icons.email_outlined),
              ctrl: _Email_Ctrl,),

            MyTextField(
              Name: "Password",
              hint_text: "Password",
              isPassword: true,
              isnum: false,
              icon: Icon(Icons.lock),
              ctrl: _UserPassWord_Ctrl,),

            MyTextField(
              Name: "Confirm Password",
              hint_text: "Confirm Password",
              isPassword: true,
              isnum: false,
              icon: Icon(Icons.lock),
              ctrl: _UserPassWordConfirm_Ctrl,),

            MyTextField(
              Name: "Phone",
              hint_text: "Phone",
              isPassword: false,
              isnum: true,
              icon: Icon(Icons.phone_android),
              ctrl: _UserPhone_Ctrl,),
            MyTextField(
              Name: "Age",
              hint_text: "Age",
              isPassword: false,
              isnum: true,
              icon: Icon(Icons.view_agenda_outlined),
              ctrl: _UseAge_Ctrl,),

           Container(
             padding: EdgeInsets.only(top: 20),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.spaceBetween,
               children: [
                 Expanded(
                      child: Container(
                        height: 58,
                        child: InputDecorator(
                          decoration: const InputDecoration(border: OutlineInputBorder()),
                          child: DropdownButtonHideUnderline(
                            child: ButtonTheme(
                              alignedDropdown: true,
                              child: DropdownButton(
                                hint: Text('Please choose a gender'), // Not necessary for Option 1
                                value: _gender,
                                onChanged: (newValue) {
                                  setState(() {
                                    _gender = newValue;
                                    print(_gender);
                                  });
                                },
                                items: gender.map((location) {
                                  return DropdownMenuItem(
                                    child: new Text(location),
                                    value: location,
                                  );
                                }).toList(),
                              )
                            ),
                          ),
                        ),
                      ),
                    ),
               ],
             ),
           ),


            Container(
              padding: EdgeInsets.only(top: 20),
              child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                  Expanded(
                    child: Container(
                      height: 58,
                      child: InputDecorator(
                        decoration: const InputDecoration(border: OutlineInputBorder()),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<int>(
                              hint: Center(child: Text('select type')),
                              items: type.map((description, value) {
                                return MapEntry(description,
                                    DropdownMenuItem<int>(
                                      value: value,
                                      child: Text(description),
                                    ));
                              }).values.toList(),
                              value: _type,
                              onChanged: (int newValue) {
                                if (newValue != null) {
                                  setState(() {
                                    _type = newValue;
                                    print(_type);
                                  });
                                }
                              },
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),


          ],
        )

    );
  }

  Widget _buildBottomPart() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10),
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          _buildAllTextFormField(),
          SizedBox(
            height: 10,
          ),
          isLoading == false
              ? Container(
            margin: EdgeInsets.only(top: 20),
            height: 45,
            width: double.infinity,
            child: MyButton(
              title: "Register",
              function: vaildation,
              mdw: 50,
            ),

          )
              : Center(
            child: CircularProgressIndicator(),
          ),

          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [

              Text('Already have account ?', style: TextStyle(fontSize: 15,
                  fontWeight: FontWeight.normal),),
              SizedBox(width: 10,),
              GestureDetector(
                onTap:(){ Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuilContextctx)=> Login()), (route) => false);},
                child: Text('Login', style: TextStyle(
                    color: MainColor,
                    fontWeight: FontWeight.bold),),
              ),
              SizedBox(height: 40,width: 10,),


            ],
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text('Sign Up'),
        centerTitle: true,
        backgroundColor: MainColor,
      ),
      body: ListView(
          children: [
            Container(
              child: Column(
                children: [
                  _buildBottomPart()

                ],
              ),
            )
          ]


      ),

    );
  }

  void vaildation() async {
    if (_UserName_Ctrl.text.isEmpty &&
        _UserName_Ctrl.text.isEmpty &&
        _UserPassWord_Ctrl.text.isEmpty &&
        _UserPhone_Ctrl.text.isEmpty &&
     _gender==null) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("please fill All fields "),
        ),
      );
    }
    else if (_UserName_Ctrl.text.isEmpty) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("please enter your name"),
        ),
      );
    } else if (_UserName_Ctrl.text.length < 6) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("name too short"),
        ),
      );
    }else if (_Email_Ctrl.text.isEmpty ) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("please enter your email"),
        ),
      );
    } else if (_UserPassWord_Ctrl.text.isEmpty) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("please enter password"),
        ),
      );
    } else if (_UserPassWord_Ctrl.text.length < 8) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("password should be 8 character"),
        ),
      );
    }else if (_UserPassWordConfirm_Ctrl.text.isEmpty) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("please enter password confirm"),
        ),
      );
    } else if (_UserPassWordConfirm_Ctrl.text.length < 8) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("confirm password should be 8 character"),
        ),
      );
    }else if (_UserPassWord_Ctrl.text  != _UserPassWordConfirm_Ctrl.text) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("passwords not match"),
        ),
      );
    } else if (_UserPhone_Ctrl.text.length < 10 || _UserPhone_Ctrl.text.length > 10) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("phone should be 10 numbers"),
        ),
      );
    } else if (_UseAge_Ctrl.text.isEmpty) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("please enter Age"),
        ),
      );
    } else if (_gender==null) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("please choose gender"),
        ),
      );
    }
    else if (_type==null) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("please choose type"),
        ),
      );
    } else {
          submit(
              FullName:_UserName_Ctrl.text,
              Phone:  _UserPhone_Ctrl.text,
              Email:  _Email_Ctrl.text,
              Gender: _gender,
              Password: _UserPassWordConfirm_Ctrl.text,
              Age:  int.parse(_UseAge_Ctrl.text),
              Rule: _type
          );
        }
    }


   void submit({String FullName,String Phone,String Email,String Gender,String Password,int Age,int Rule}) async
   {
     setState(() {isLoading = true;});
     DioUtil.createDio().post("UsersRigester/Insert",data: {
       "Name": "$FullName",
       "Phone": "$Phone",
       "Email": "$Email",
       "Gender": "$Gender",
       "Password": "$Password",
       "PhotoUrl": "image.png",
       "Age": Age,
       "Rule": Rule
     }).then((response)
     {
       if(response.statusCode==200)
       {
       setState(() {isLoading = false;});
       okAlter(context, "done", "Register succeeded");
       Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (ctx) => Login()));
       }
       else
       {
       setState(() {isLoading = false;});
       scaffoldKey.currentState.showSnackBar(
       SnackBar(content: Text("Connection Error"),),
       );
       }
     } );

   }

   Future<void> okAlter(BuildContext context,String title,String contain) {
     return showDialog<void>(
       context: context,
       builder: (BuildContext context) {
         return AlertDialog(
           title: Text(title),
           content: Text(contain),
           actions: <Widget>[
             FlatButton(
               child: Text('OK'),
               onPressed: () {
                 Navigator.of(context).pop();
               },
             ),
           ],
         );
       },
     );
   }
  }




